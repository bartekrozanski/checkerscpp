#include "HumanPlayer.h"



HumanPlayer::HumanPlayer(Player player, MouseHandler& mouseHandler):
	BasePlayer(player),
	mouseHandler(mouseHandler)
{
}


HumanPlayer::~HumanPlayer()
{
}

Move HumanPlayer::ChooseMove(const Board& board, const Moves & moves)
{
	Move requestedMove = mouseHandler.HandleInput();
	for (auto it = moves.cbegin(); it != moves.cend(); ++it)
	{
		if (it->from == requestedMove.from && it->to == requestedMove.to)
		{
			return *it;
		}
	}
	return Move::InvalidMove;
}
