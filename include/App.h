#pragma once
#include <iostream>
#include "Board.h"
#include "MoveGenerator.h"
#include "BasePlayer.h"
#include "Window.h"
#include "Game.h"
class App
{
public:
	App(Game& game,
		Window& window);

	~App();

	Player MainLoop();
	void DisplayWhoWon();
	void SetTimeLimitForGame(int seconds);
	void ResetGame(Player startingPlayer);


private:
	int timeLimit;
	Game game;
	void HandleInput();
	Window& window;
	sf::Clock clock;
};

