#include "Simulation.cuh"


//#define DEBUG



__device__ int isMainThread(int tid)
{
	return ((tid % 32) == 0 ? 1 : 0);
}


__device__ __forceinline__ int getIndex(int thread_id,
	int logical_index)
{
	int offset = (thread_id / THREADS_PER_GAME)*MAX_FIELDS*MAX_MOVES;
	return offset + (logical_index * MAX_FIELDS) + (thread_id%THREADS_PER_GAME);
}

__device__ __forceinline__ int getTidFromIndex(int physicalIndex)
{
	int whichWarp = (physicalIndex / (MAX_FIELDS * MAX_MOVES));
	int relativeTid = (physicalIndex - (whichWarp * THREADS_PER_GAME)) % 32;

	return (whichWarp * THREADS_PER_GAME) + relativeTid;
}

__device__ __forceinline__ int getIndex(int thread_id)
{
	return thread_id / THREADS_PER_GAME;
}

__device__ __forceinline__ int getOffset(int thread_id)
{
	return (thread_id / THREADS_PER_GAME)*THREADS_PER_GAME;
}
__device__ int isEvenRow(int index)
{
	return index % 8 < 4;
}

float SimulateCuda(int board[],
	curandState *rngState,
	int numOfSimulations,
	int myPlayer,
	int whoseTurn,
	int isCapturingTurn)
{
	int numOfBlocks = NUMBER_OF_BLOCKS;
	int gamesPerBlock = THREADBLOCK_SIZE / THREADS_PER_GAME;
	cudaError_t cudaStatus;
	float *dev_winRatios = nullptr;
	int *dev_board = nullptr;

	float *host_winRatios = nullptr;
	host_winRatios = new float[numOfBlocks*gamesPerBlock];

	cudaStatus = cudaSetDevice(0);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaSetDevice failed!  Do you have a CUDA-capable GPU installed?");
	}

	cudaStatus = cudaMalloc((void**)&dev_winRatios, numOfBlocks * gamesPerBlock * sizeof(float));
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
	}
	cudaStatus = cudaMalloc((void**)&dev_board,sizeof(int) * MAX_FIELDS);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMalloc failed!");
	}

	cudaStatus = cudaMemcpy(dev_board, board, MAX_FIELDS * sizeof(int), cudaMemcpyHostToDevice);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
	}

	simulateKernel << <numOfBlocks, THREADBLOCK_SIZE>> > (dev_board, rngState, (numOfSimulations/gamesPerBlock)/numOfBlocks,dev_winRatios, myPlayer, whoseTurn, isCapturingTurn);

	cudaStatus = cudaGetLastError();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "addKernel launch failed: %s\n", cudaGetErrorString(cudaStatus));
	}
	cudaStatus = cudaDeviceSynchronize();
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaDeviceSynchronize returned error code %d after launching addKernel!\n", cudaStatus);
	}

	cudaStatus = cudaMemcpy(host_winRatios, dev_winRatios, numOfBlocks* gamesPerBlock * sizeof(float), cudaMemcpyDeviceToHost);
	if (cudaStatus != cudaSuccess) {
		fprintf(stderr, "cudaMemcpy failed!");
	}

	cudaFree(dev_winRatios);
	float winRatio = 0;
	for (int i = 0; i < numOfBlocks * gamesPerBlock; ++i)
	{
		winRatio += host_winRatios[i];

	}

	 winRatio /= (numOfBlocks * gamesPerBlock);

	delete[] host_winRatios;
	return winRatio;
}




__device__ int getNorthWestIndex(int index)
{
	index %= THREADS_PER_GAME;
	if ((index - 4) % 8 == 0 || index < 4) return INVALID_INDEX;
	if (isEvenRow(index)) return index - 4;
	return index - 5;
}

__device__ int getNorthEastIndex(int index)
{
	index %= THREADS_PER_GAME;

	if ((index - 3) % 8 == 0 || index < 4) return INVALID_INDEX;
	if (isEvenRow(index)) return index - 3;
	return index - 4;
}

__device__ int getSouthWestIndex(int index)
{
	index %= THREADS_PER_GAME;

	if ((index - 4) % 8 == 0 || index > 27) return INVALID_INDEX;
	if (isEvenRow(index)) return index + 4;
	return index + 3;
}

__device__ int getSouthEastIndex(int index)
{
	index %= THREADS_PER_GAME;

	if ((index - 3) % 8 == 0 || index > 27) return INVALID_INDEX;
	if (isEvenRow(index)) return index + 5;
	return index + 4;
}

void InitializeRNG(int seed, curandState *rngState)
{
	initializeRng << <1, NUMBER_OF_BLOCKS * NUM_OF_WARPS >> > (seed, rngState);
}


__global__ void initializeRng(int seed, curandState *rngState)
{
	int tid = threadIdx.x;
	curand_init(seed + tid, 0, 0, &(rngState[tid]));
}

__global__ void simulateKernel(int *dev_board,
	curandState* rngState,
	int numOfSimulations,
	float* dev_winRatios,
	int myPlayer,
	int whoseTurn,
	int isCapturingTurn)
{
	//declarations of shared memory
	__shared__ int indexOfMove[NUM_OF_WARPS];
	__shared__ int numOfMoves[NUM_OF_WARPS];
	__shared__ int numOfWins[NUM_OF_WARPS];
	__shared__ int capturingTurn[NUM_OF_WARPS];
	__shared__ int currentPlayer[NUM_OF_WARPS];
	__shared__ int endOfGame[NUM_OF_WARPS];
	__shared__ int originalPlayer[NUM_OF_WARPS];
	__shared__ int lastCapturingPiece[NUM_OF_WARPS];


	__shared__ int to[MAX_FIELDS * MAX_MOVES * NUM_OF_WARPS];
	__shared__ int capturedPiece[MAX_FIELDS * MAX_MOVES * NUM_OF_WARPS];
	__shared__ int currentBoard[MAX_FIELDS* NUM_OF_WARPS];
	__shared__ int originalBoard[MAX_FIELDS * NUM_OF_WARPS];

	//copying board to orginalBoard (to make use of shared memory and with every iteration
	//to use only this memory, not local one). Storing current board

	int tid = threadIdx.x;
	int sharedIndex = getIndex(tid);
	int offset = getOffset(tid);
	if (isMainThread(tid))
	{
		numOfWins[sharedIndex] = 0;
		originalPlayer[sharedIndex] = whoseTurn;
		lastCapturingPiece[sharedIndex] = INVALID_INDEX;
	}

	originalBoard[tid] = dev_board[tid%THREADS_PER_GAME];

	int generatedMoves = 0;
	int neighbourIndex;
	int neighbourPiece;
	int neighbourIndexBehind;
	int neighbourPieceBehind;
	int physicalIndex;
	int ply = 1;
	//initialize shared memory


	for (int iteration = 0; iteration < numOfSimulations; ++iteration)
	{
		//simulation setup

		if (isMainThread(tid))
		{
			currentPlayer[sharedIndex] = originalPlayer[sharedIndex];
			capturingTurn[sharedIndex] = isCapturingTurn;
			endOfGame[sharedIndex] = 0;

		}
		currentBoard[tid] = originalBoard[tid];

		while (ply)
		{
			ply++;

#ifdef DEBUG
			__syncthreads();

			////// SHOW BOARD
			if (tid == 0)
			{
				printf("\n");

				printf("#####PLY  %d ######### from tid %d \n", ply, tid);
				for (int i = tid; i < tid + MAX_FIELDS; ++i)
				{
					if (i % 8 >= 4) //even row
					{
						printf("%d ", currentBoard[i]);
					}
					else
					{
						printf(" %d", currentBoard[i]);
					}
					if ((i - 3) % 4 == 0) printf("\n");
				}
			}
			__syncthreads();
			if (tid == 32)
			{
				printf("\n");
				printf("#####PLY  %d ######### from tid %d \n", ply, tid);
				for (int i = tid; i < tid + MAX_FIELDS; ++i)
				{
					if (i % 8 >= 4) //even row
					{
						printf("%d ", currentBoard[i]);
					}
					else
					{
						printf(" %d", currentBoard[i]);
					}
					if ((i - 3) % 4 == 0) printf("\n");
				}
			}
			__syncthreads();

#endif // DEBUG


			



			/// MOVE GENERATION
			//clearing move list
			generatedMoves = 0;

			for (int i = 0; i < MAX_MOVES; ++i)
			{
				physicalIndex = getIndex(tid, i);
				to[physicalIndex] = INVALID_INDEX;
				capturedPiece[physicalIndex] = INVALID_INDEX;
			}
			if (isMainThread(tid))
			{
				numOfMoves[sharedIndex] = 0;
				indexOfMove[sharedIndex] = INVALID_INDEX;
			}

			if (currentPlayer[sharedIndex] == BLACK_PLAYER)
			{
				if (capturingTurn[sharedIndex])
				{
					//generate only acpturing moves for previously capturing piece
					if (tid == lastCapturingPiece[sharedIndex])
					{
						if (currentBoard[tid] == BLACK_PAWN || currentBoard[tid] == BLACK_KING)
						{
							//generate moves
							//Check if there exists capturing move in NorthWest direction

							neighbourIndex = getNorthWestIndex(tid);
							if (neighbourIndex != INVALID_INDEX)
							{
								neighbourPiece = currentBoard[neighbourIndex + offset];
								neighbourIndexBehind = getNorthWestIndex(neighbourIndex);
								if (neighbourIndexBehind != INVALID_INDEX)
								{
									neighbourPieceBehind = currentBoard[neighbourIndexBehind + offset];

									if ((neighbourPiece == RED_PAWN || neighbourPiece == RED_KING) && neighbourPieceBehind == EMPTY)
									{
										//capturing move exists
										physicalIndex = getIndex(tid, 0);
										to[physicalIndex] = neighbourIndexBehind;
										capturedPiece[physicalIndex] = neighbourIndex;
										generatedMoves++;
									}
								}
							}



							//Check if there exists capturing move in NorthEast direction

							neighbourIndex = getNorthEastIndex(tid);
							if (neighbourIndex != INVALID_INDEX)
							{
								neighbourPiece = currentBoard[neighbourIndex + offset];
								neighbourIndexBehind = getNorthEastIndex(neighbourIndex);
								if (neighbourIndexBehind != INVALID_INDEX)
								{
									neighbourPieceBehind = currentBoard[neighbourIndexBehind + offset];

									if ((neighbourPiece == RED_PAWN || neighbourPiece == RED_KING) && neighbourPieceBehind == EMPTY)
									{
										//capturing move exists
										physicalIndex = getIndex(tid, 1);

										to[physicalIndex] = neighbourIndexBehind;
										capturedPiece[physicalIndex] = neighbourIndex;
										generatedMoves++;
									}
								}
							}



						}

						if (currentBoard[tid] == BLACK_KING)
						{
							//Check if there exists capturing move in SouthWest direction

							neighbourIndex = getSouthWestIndex(tid);
							if (neighbourIndex != INVALID_INDEX)
							{

								neighbourPiece = currentBoard[neighbourIndex + offset];
								neighbourIndexBehind = getSouthWestIndex(neighbourIndex);
								if (neighbourIndexBehind != INVALID_INDEX)
								{
									neighbourPieceBehind = currentBoard[neighbourIndexBehind + offset];

									if ((neighbourPiece == RED_PAWN || neighbourPiece == RED_KING) && neighbourPieceBehind == EMPTY)
									{
										//capturing move exists
										physicalIndex = getIndex(tid, 2);

										to[physicalIndex] = neighbourIndexBehind;
										capturedPiece[physicalIndex] = neighbourIndex;
										generatedMoves++;
									}
								}

							}

							//Check if there exists capturing move in SouthEast direction

							neighbourIndex = getSouthEastIndex(tid);
							if (neighbourIndex != INVALID_INDEX)
							{
								neighbourPiece = currentBoard[neighbourIndex + offset];
								neighbourIndexBehind = getSouthEastIndex(neighbourIndex);
								if (neighbourIndexBehind != INVALID_INDEX)
								{
									neighbourPieceBehind = currentBoard[neighbourIndexBehind + offset];

									if ((neighbourPiece == RED_PAWN || neighbourPiece == RED_KING) && neighbourPieceBehind == EMPTY)
									{
										//capturing move exists
										int physicalIndex = getIndex(tid, 3);

										to[physicalIndex] = neighbourIndexBehind;
										capturedPiece[physicalIndex] = neighbourIndex;
										generatedMoves++;
									}
								}
							}


						}

						atomicAdd(&(numOfMoves[sharedIndex]), generatedMoves);
					}
				}
				else
				{
					//generate capturing moves
					//black player
					if (currentBoard[tid] == BLACK_PAWN || currentBoard[tid] == BLACK_KING)
					{
						//generate moves
						//Check if there exists capturing move in NorthWest direction

						neighbourIndex = getNorthWestIndex(tid);
						if (neighbourIndex != INVALID_INDEX)
						{
							neighbourPiece = currentBoard[neighbourIndex + offset];
							neighbourIndexBehind = getNorthWestIndex(neighbourIndex);
							if (neighbourIndexBehind != INVALID_INDEX)
							{
								neighbourPieceBehind = currentBoard[neighbourIndexBehind + offset];

								if ((neighbourPiece == RED_PAWN || neighbourPiece == RED_KING) && neighbourPieceBehind == EMPTY)
								{
									//capturing move exists
									physicalIndex = getIndex(tid, 0);
									to[physicalIndex] = neighbourIndexBehind;
									capturedPiece[physicalIndex] = neighbourIndex;
									generatedMoves++;
								}
							}
						}



						//Check if there exists capturing move in NorthEast direction

						neighbourIndex = getNorthEastIndex(tid);
						if (neighbourIndex != INVALID_INDEX)
						{
							neighbourPiece = currentBoard[neighbourIndex + offset];
							neighbourIndexBehind = getNorthEastIndex(neighbourIndex);
							if (neighbourIndexBehind != INVALID_INDEX)
							{
								neighbourPieceBehind = currentBoard[neighbourIndexBehind + offset];

								if ((neighbourPiece == RED_PAWN || neighbourPiece == RED_KING) && neighbourPieceBehind == EMPTY)
								{
									//capturing move exists
									physicalIndex = getIndex(tid, 1);

									to[physicalIndex] = neighbourIndexBehind;
									capturedPiece[physicalIndex] = neighbourIndex;
									generatedMoves++;
								}
							}
						}



					}

					if (currentBoard[tid] == BLACK_KING)
					{
						//Check if there exists capturing move in SouthWest direction

						neighbourIndex = getSouthWestIndex(tid);
						if (neighbourIndex != INVALID_INDEX)
						{

							neighbourPiece = currentBoard[neighbourIndex + offset];
							neighbourIndexBehind = getSouthWestIndex(neighbourIndex);
							if (neighbourIndexBehind != INVALID_INDEX)
							{
								neighbourPieceBehind = currentBoard[neighbourIndexBehind + offset];

								if ((neighbourPiece == RED_PAWN || neighbourPiece == RED_KING) && neighbourPieceBehind == EMPTY)
								{
									//capturing move exists
									physicalIndex = getIndex(tid, 2);

									to[physicalIndex] = neighbourIndexBehind;
									capturedPiece[physicalIndex] = neighbourIndex;
									generatedMoves++;
								}
							}

						}

						//Check if there exists capturing move in SouthEast direction

						neighbourIndex = getSouthEastIndex(tid);
						if (neighbourIndex != INVALID_INDEX)
						{
							neighbourPiece = currentBoard[neighbourIndex + offset];
							neighbourIndexBehind = getSouthEastIndex(neighbourIndex);
							if (neighbourIndexBehind != INVALID_INDEX)
							{
								neighbourPieceBehind = currentBoard[neighbourIndexBehind + offset];

								if ((neighbourPiece == RED_PAWN || neighbourPiece == RED_KING) && neighbourPieceBehind == EMPTY)
								{
									//capturing move exists
									int physicalIndex = getIndex(tid, 3);

									to[physicalIndex] = neighbourIndexBehind;
									capturedPiece[physicalIndex] = neighbourIndex;
									generatedMoves++;
								}
							}
						}


					}

					atomicAdd(&(numOfMoves[sharedIndex]), generatedMoves);




					if (numOfMoves[sharedIndex] == 0) //there are no capturing moves, checking normal moves
					{
						if (currentBoard[tid] == BLACK_PAWN || currentBoard[tid] == BLACK_KING)
						{
							//generate moves
							//Check if there exists normal move in NorthWest direction

							neighbourIndex = getNorthWestIndex(tid);
							if (neighbourIndex != INVALID_INDEX)
							{
								neighbourPiece = currentBoard[neighbourIndex + offset];
								if (neighbourPiece == EMPTY)
								{

									//simple move exists
									physicalIndex = getIndex(tid, 0);

									to[physicalIndex] = neighbourIndex;
									generatedMoves++;
								}
							}



							//Check if there exists normal move in NorthEast direction

							neighbourIndex = getNorthEastIndex(tid);
							if (neighbourIndex != INVALID_INDEX)
							{

								neighbourPiece = currentBoard[neighbourIndex + offset];

								if (neighbourPiece == EMPTY)
								{
									physicalIndex = getIndex(tid, 1);

									to[physicalIndex] = neighbourIndex;
									generatedMoves++;
								}
							}


						}
						if (currentBoard[tid] == BLACK_KING)
						{
							//Check if there exists normal move in SouthWest direction

							neighbourIndex = getSouthWestIndex(tid);
							if (neighbourIndex != INVALID_INDEX)
							{
								neighbourPiece = currentBoard[neighbourIndex + offset];
								if (neighbourPiece == EMPTY)
								{
									//capturing move exists
									physicalIndex = getIndex(tid, 2);

									to[physicalIndex] = neighbourIndex;
									generatedMoves++;
								}
							}


							//Check if there exists normal move in SouthEast direction

							neighbourIndex = getSouthEastIndex(tid);
							if (neighbourIndex != INVALID_INDEX)
							{
								neighbourPiece = currentBoard[neighbourIndex + offset];
								if (neighbourPiece == EMPTY)
								{
									//capturing move exists
									int physicalIndex = getIndex(tid, 3);

									to[physicalIndex] = neighbourIndex;
									generatedMoves++;
								}
							}
						}

						atomicAdd(&(numOfMoves[sharedIndex]), generatedMoves);
					}
					//printf("player: %d, tid: %d, GENERATEDMOVES -> %d / %d\n", currentPlayer, tid, generatedMoves, numOfMoves);
					//if (turn > 4)while (true);
				}
			}

				
			else
			{
				if (capturingTurn[sharedIndex])
				{
					if (tid == lastCapturingPiece[sharedIndex])
					{
						//generate capturing moves
				//red player

						if (currentBoard[tid] == RED_PAWN || currentBoard[tid] == RED_KING)
						{
							//generate moves

							//Check if there exists capturing move in SouthWest direction

							neighbourIndex = getSouthWestIndex(tid);
							if (neighbourIndex != INVALID_INDEX)
							{
								neighbourPiece = currentBoard[neighbourIndex + offset];
								neighbourIndexBehind = getSouthWestIndex(neighbourIndex);
								if (neighbourIndexBehind != INVALID_INDEX)
								{
									neighbourPieceBehind = currentBoard[neighbourIndexBehind + offset];

									if ((neighbourPiece == BLACK_PAWN || neighbourPiece == BLACK_KING) && neighbourPieceBehind == EMPTY)
									{
										//capturing move exists
										physicalIndex = getIndex(tid, 2);

										to[physicalIndex] = neighbourIndexBehind;
										capturedPiece[physicalIndex] = neighbourIndex;
										generatedMoves++;
									}
								}
							}
							//Check if there exists capturing move in SouthEast direction

							neighbourIndex = getSouthEastIndex(tid);
							if (neighbourIndex != INVALID_INDEX)
							{
								neighbourPiece = currentBoard[neighbourIndex + offset];
								neighbourIndexBehind = getSouthEastIndex(neighbourIndex);
								if (neighbourIndexBehind != INVALID_INDEX)
								{
									neighbourPieceBehind = currentBoard[neighbourIndexBehind + offset];

									if ((neighbourPiece == BLACK_PAWN || neighbourPiece == BLACK_KING) && neighbourPieceBehind == EMPTY)
									{
										//capturing move exists
										int physicalIndex = getIndex(tid, 3);
										to[physicalIndex] = neighbourIndexBehind;
										capturedPiece[physicalIndex] = neighbourIndex;
										generatedMoves++;
									}
								}
							}


						}

						if (currentBoard[tid] == RED_KING)
						{
							//Check if there exists capturing move in NorthWest direction

							neighbourIndex = getNorthWestIndex(tid);
							if (neighbourIndex != INVALID_INDEX)
							{
								neighbourPiece = currentBoard[neighbourIndex + offset];
								neighbourIndexBehind = getNorthWestIndex(neighbourIndex);
								if (neighbourIndexBehind != INVALID_INDEX)
								{
									neighbourPieceBehind = currentBoard[neighbourIndexBehind + offset];

									if ((neighbourPiece == BLACK_PAWN || neighbourPiece == BLACK_KING) && neighbourPieceBehind == EMPTY)
									{
										//capturing move exists
										physicalIndex = getIndex(tid, 0);
										to[physicalIndex] = neighbourIndexBehind;
										capturedPiece[physicalIndex] = neighbourIndex;
										generatedMoves++;
									}
								}
							}



							//Check if there exists capturing move in NorthEast direction

							neighbourIndex = getNorthEastIndex(tid);
							if (neighbourIndex != INVALID_INDEX)
							{
								neighbourPiece = currentBoard[neighbourIndex + offset];
								neighbourIndexBehind = getNorthEastIndex(neighbourIndex);
								if (neighbourIndexBehind != INVALID_INDEX)
								{
									neighbourPieceBehind = currentBoard[neighbourIndexBehind + offset];

									if ((neighbourPiece == BLACK_PAWN || neighbourPiece == BLACK_KING) && neighbourPieceBehind == EMPTY)
									{
										//capturing move exists
										physicalIndex = getIndex(tid, 1);
										to[physicalIndex] = neighbourIndexBehind;
										capturedPiece[physicalIndex] = neighbourIndex;
										generatedMoves++;
									}
								}
							}
						}

						atomicAdd(&(numOfMoves[sharedIndex]), generatedMoves);
					}

				}
				else
				{
					//generate capturing moves
				//red player

					if (currentBoard[tid] == RED_PAWN || currentBoard[tid] == RED_KING)
					{
						//generate moves

						//Check if there exists capturing move in SouthWest direction

						neighbourIndex = getSouthWestIndex(tid);
						if (neighbourIndex != INVALID_INDEX)
						{
							neighbourPiece = currentBoard[neighbourIndex + offset];
							neighbourIndexBehind = getSouthWestIndex(neighbourIndex);
							if (neighbourIndexBehind != INVALID_INDEX)
							{
								neighbourPieceBehind = currentBoard[neighbourIndexBehind + offset];

								if ((neighbourPiece == BLACK_PAWN || neighbourPiece == BLACK_KING) && neighbourPieceBehind == EMPTY)
								{
									//capturing move exists
									physicalIndex = getIndex(tid, 2);

									to[physicalIndex] = neighbourIndexBehind;
									capturedPiece[physicalIndex] = neighbourIndex;
									generatedMoves++;
								}
							}
						}
						//Check if there exists capturing move in SouthEast direction

						neighbourIndex = getSouthEastIndex(tid);
						if (neighbourIndex != INVALID_INDEX)
						{
							neighbourPiece = currentBoard[neighbourIndex + offset];
							neighbourIndexBehind = getSouthEastIndex(neighbourIndex);
							if (neighbourIndexBehind != INVALID_INDEX)
							{
								neighbourPieceBehind = currentBoard[neighbourIndexBehind + offset];

								if ((neighbourPiece == BLACK_PAWN || neighbourPiece == BLACK_KING) && neighbourPieceBehind == EMPTY)
								{
									//capturing move exists
									int physicalIndex = getIndex(tid, 3);
									to[physicalIndex] = neighbourIndexBehind;
									capturedPiece[physicalIndex] = neighbourIndex;
									generatedMoves++;
								}
							}
						}


					}

					if (currentBoard[tid] == RED_KING)
					{
						//Check if there exists capturing move in NorthWest direction

						neighbourIndex = getNorthWestIndex(tid);
						if (neighbourIndex != INVALID_INDEX)
						{
							neighbourPiece = currentBoard[neighbourIndex + offset];
							neighbourIndexBehind = getNorthWestIndex(neighbourIndex);
							if (neighbourIndexBehind != INVALID_INDEX)
							{
								neighbourPieceBehind = currentBoard[neighbourIndexBehind + offset];

								if ((neighbourPiece == BLACK_PAWN || neighbourPiece == BLACK_KING) && neighbourPieceBehind == EMPTY)
								{
									//capturing move exists
									physicalIndex = getIndex(tid, 0);
									to[physicalIndex] = neighbourIndexBehind;
									capturedPiece[physicalIndex] = neighbourIndex;
									generatedMoves++;
								}
							}
						}



						//Check if there exists capturing move in NorthEast direction

						neighbourIndex = getNorthEastIndex(tid);
						if (neighbourIndex != INVALID_INDEX)
						{
							neighbourPiece = currentBoard[neighbourIndex + offset];
							neighbourIndexBehind = getNorthEastIndex(neighbourIndex);
							if (neighbourIndexBehind != INVALID_INDEX)
							{
								neighbourPieceBehind = currentBoard[neighbourIndexBehind + offset];

								if ((neighbourPiece == BLACK_PAWN || neighbourPiece == BLACK_KING) && neighbourPieceBehind == EMPTY)
								{
									//capturing move exists
									physicalIndex = getIndex(tid, 1);
									to[physicalIndex] = neighbourIndexBehind;
									capturedPiece[physicalIndex] = neighbourIndex;
									generatedMoves++;
								}
							}
						}
					}

					atomicAdd(&(numOfMoves[sharedIndex]), generatedMoves);


					if (numOfMoves[sharedIndex] == 0) //there are no capturing moves, checking normal moves
					{
						if (currentBoard[tid] == RED_PAWN || currentBoard[tid] == RED_KING)
						{
							//generate moves

							//Check if there exists normal move in SouthWest direction

							neighbourIndex = getSouthWestIndex(tid);
							if (neighbourIndex != INVALID_INDEX)
							{
								neighbourPiece = currentBoard[neighbourIndex + offset];
								if (neighbourPiece == EMPTY)
								{
									physicalIndex = getIndex(tid, 2);
									to[physicalIndex] = neighbourIndex;
									generatedMoves++;
								}
							}
							//Check if there exists normal move in SouthEast direction

							neighbourIndex = getSouthEastIndex(tid);
							if (neighbourIndex != INVALID_INDEX)
							{
								neighbourPiece = currentBoard[neighbourIndex + offset];
								if (neighbourPiece == EMPTY)
								{
									int physicalIndex = getIndex(tid, 3);
									to[physicalIndex] = neighbourIndex;
									generatedMoves++;
								}
							}
						}

						if (currentBoard[tid] == RED_KING)
						{
							//Check if there exists normal move in NorthWest direction

							neighbourIndex = getNorthWestIndex(tid);
							if (neighbourIndex != INVALID_INDEX)
							{
								neighbourPiece = currentBoard[neighbourIndex + offset];
								if (neighbourPiece == EMPTY)
								{
									//simple move exists
									physicalIndex = getIndex(tid, 0);
									to[physicalIndex] = neighbourIndex;
									generatedMoves++;
								}
							}

							//Check if there exists normal move in NorthEast direction

							neighbourIndex = getNorthEastIndex(tid);
							if (neighbourIndex != INVALID_INDEX)
							{
								neighbourPiece = currentBoard[neighbourIndex + offset];

								if (neighbourPiece == EMPTY)
								{
									physicalIndex = getIndex(tid, 1);
									to[physicalIndex] = neighbourIndex;
									generatedMoves++;
								}
							}
						}

						atomicAdd(&(numOfMoves[sharedIndex]), generatedMoves);

					}
				}
				}



				

			///we know all possible moves and how many are there of them
			//game logic
#ifdef DEBUG
			__syncthreads();
			printf("player: %d, tid: %d, GENERATEDMOVES -> %d / %d\n", currentPlayer[sharedIndex],tid, generatedMoves, numOfMoves[sharedIndex]);
			__syncthreads();


#endif // DEBUG



			//while (true);
			if (isMainThread(tid))
			{
				//check if someone  won
				if (numOfMoves[sharedIndex] == 0 && capturingTurn[sharedIndex] == 0) //current player lost
				{
					endOfGame[sharedIndex] = 1;
				}
			}
			if (endOfGame[sharedIndex] == 1) break;

			if (isMainThread(tid))
			{

				if (numOfMoves[sharedIndex] != 0)
				{
					//XXX(tid);

					int moveToChoose = (curand(&(rngState[sharedIndex])) % numOfMoves[sharedIndex])+1;
					int counter = 0;
					indexOfMove[sharedIndex] = 0;

					for (int i = (tid/THREADS_PER_GAME)*(MAX_FIELDS * MAX_MOVES); i < ((tid/THREADS_PER_GAME)+1)* MAX_FIELDS * MAX_MOVES ; ++i)
					{
						if (to[i] != INVALID_INDEX)
						{
							counter++;
							if (counter == moveToChoose)
							{
								indexOfMove[sharedIndex] = i;
#ifdef DEBUG
								printf("tid: %d, indexofMove: %d, moveToChose: %d\n",tid, indexOfMove[sharedIndex], moveToChoose);

#endif // DEBUG

								break;
							}
						}
					}

					//execute chosen move
#ifdef DEBUG

					printf("tid:%d Executing from:%d to:%d capturedPiece:%d\n",tid, getTidFromIndex(indexOfMove[sharedIndex])%THREADS_PER_GAME , to[indexOfMove[sharedIndex]], capturedPiece[indexOfMove[sharedIndex]]);
#endif // DEBUG


					currentBoard[to[indexOfMove[sharedIndex]] + tid] = currentBoard[getTidFromIndex(indexOfMove[sharedIndex])];
					currentBoard[getTidFromIndex(indexOfMove[sharedIndex])] = EMPTY;
					if (capturedPiece[indexOfMove[sharedIndex]] != INVALID_INDEX)
					{
						capturingTurn[sharedIndex] = 1;
						lastCapturingPiece[sharedIndex] = to[indexOfMove[sharedIndex]] + tid;
						currentBoard[capturedPiece[indexOfMove[sharedIndex]] + tid] = EMPTY;

					}
					else
					{
						capturingTurn[sharedIndex] = 0;
						if (currentPlayer[sharedIndex] == BLACK_PLAYER) currentPlayer[sharedIndex] = RED_PLAYER;
						else currentPlayer[sharedIndex] = BLACK_PLAYER;
					}
				}
				else
				{
					capturingTurn[sharedIndex] = 0;
					if (currentPlayer[sharedIndex] == BLACK_PLAYER) currentPlayer[sharedIndex] = RED_PLAYER;
					else currentPlayer[sharedIndex] = BLACK_PLAYER;
				}

			}
			if (tid%THREADS_PER_GAME < 4 && currentBoard[tid] == BLACK_PAWN)
			{
				currentBoard[tid] = BLACK_KING;
			}
			if (tid%THREADS_PER_GAME > 27 && currentBoard[tid] == RED_PAWN)
			{
				currentBoard[tid] = RED_KING;
			}
		}
		if (isMainThread(tid) && currentPlayer[sharedIndex] != myPlayer)
		{
			(numOfWins[sharedIndex])++;
		}
	}
	if (isMainThread(tid))
	{
		dev_winRatios[blockIdx.x*(THREADBLOCK_SIZE/THREADS_PER_GAME) + sharedIndex] = (float)numOfWins[sharedIndex] / (float)numOfSimulations;
	}
}
