#pragma once
#include <SFML/Graphics.hpp>
#include <string>
#include "Board.h"
#include "MoveGenerator.h"

class Window
{
public:
	Window(int width, int height, const std::string& title);
	void RedrawEverything(const Board& board, const Moves& moves);
	void DrawBoard(const Board& board);
	void DrawMoves(const Moves& moves);
	void Display();
	void Clear();
	void HandleInput();
	bool IsOpen();
	sf::Vector2i GetPosition();
	~Window();
	Index GridCoordsToIndex(int x, int y);
	void IndexToGridCoords(Index index, int& outX, int& outY);
	void ScreenCoordsToGridCoords(sf::Vector2i screenCoords, int& outX, int& outY);
	Index ScreenCoordsToIndex(sf::Vector2i screenCoords);

private:


	sf::RenderWindow window;
	int width;
	int height;
	float cellX;
	float cellY;

	sf::Sprite blackField;
	sf::Sprite whiteField;
	sf::Sprite possibleMove;
	sf::Sprite blackPawn;
	sf::Sprite redPawn;
	sf::Sprite blackKing;
	sf::Sprite redKing;

	sf::Texture blackFieldTexture;
	sf::Texture whiteFieldTexture;
	sf::Texture possibleMoveTexture;
	sf::Texture blackPawnTexture;
	sf::Texture redPawnTexture;
	sf::Texture blackKingTexture;
	sf::Texture redKingTexture;
};

//	sf::CircleShape shape(100.f);
//	shape.setFillColor(sf::Color::Green);
//
//	while (window.isOpen())
//	{
//		sf::Event event;
//		while (window.pollEvent(event))
//		{
//			if (event.type == sf::Event::Closed)
//				window.close();
//		}
//
//		window.clear();
//		window.draw(shape);
//		window.display();
//	}
//}


