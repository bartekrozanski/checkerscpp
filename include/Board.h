#pragma once
#include <cstring>

using Piece = char;
using Index = char;

//constexpr Piece OUT_OF_BOARD = 99;
//constexpr Piece BLACK_PAWN = 1;
//constexpr Piece BLACK_KING = 2;
//constexpr Piece RED_PAWN = -1;
//constexpr Piece RED_KING = -2;
//constexpr Piece EMPTY = 0;
//constexpr Index FIELDS = 32;

#define OUT_OF_BOARD  (-1)
#define BLACK_PAWN  1
#define BLACK_KING  2
#define RED_PAWN  3
#define RED_KING  4
#define EMPTY  0
#define FIELDS  32

struct Move
{
	Index from;
	Index to;
	Index capturedField;
	Move(Index from, Index to, Index capturedField = -1) :
		from(from),
		to(to),
		capturedField(capturedField)
	{}

	bool IsCapturing() const;
	static const Move InvalidMove;
};


enum Directions
{
	NE,
	NW,
	SE,
	SW,
};

class Board
{
public:
	Board() {};
	virtual ~Board() {};

	virtual Piece GetNeighbourPiece(Index index, Directions direction)const = 0;
	virtual Piece GetPieceAt(Index index) const = 0;
	virtual void Reset() = 0;
	virtual void ExecuteMove(const Move& move) = 0;
	virtual void UpgradeToKing(Index index) = 0;
	Index GetNeighbourIndex(Index index, Directions direction) const;

	virtual Board* Clone() const = 0;

protected:
	virtual void SetPieceAt(Index index, Piece newPiece) = 0;
	bool IsEvenRow(Index index) const { return index % 8 < 4; }
};

class PackedBoard :
	public Board
{
public:
	PackedBoard();
	virtual ~PackedBoard() override;
	virtual void Reset() override;

	virtual Piece GetNeighbourPiece(Index index, Directions direction) const override;
	virtual Piece GetPieceAt(Index index) const override;
	virtual void ExecuteMove(const Move& move) override;
	virtual void UpgradeToKing(Index index) override;

	virtual PackedBoard* Clone() const;
protected:
	virtual void SetPieceAt(Index index, Piece piece) override;
	Piece board[32];

};

