#pragma once
#include "MonteCarloPlayer.h"
#include "RandomPlayer.h"
#include "Game.h"
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <chrono>

class CpuMonteCarloPlayer :
	public MonteCarloPlayer
{
public:
	CpuMonteCarloPlayer(Player player, int simulationsPerMove);
	virtual ~CpuMonteCarloPlayer() override;

protected:
	virtual double Simulate(const Game& game, int iterations) override;
};

