#include "App.h"
#include "Window.h"
#include "HumanPlayer.h"
#include "CpuMonteCarloPlayer.h"
#include "GpuMonteCarloPlayer.h"
#include <memory>
#include <limits>

void usage(std::string appName)
{
	std::cerr << "Usage: " << appName << "[player type] [player type] [number of simulations] [number of simulations] [time limit]\n";
	std::cerr << "[player type] one from [gpu|cpu|human]. First one for red player, second for black.\n";
	std::cerr << "[number of simulations] integer number. First one for red player, second for black. If player is human player then pass here any number.\n";
	std::cerr << "[time limit] integer number. Time in seconds after game will end with draw result. Useful for AI vs AI games.\n";
}

int main(int argc, char **argv)
{
	std::srand(std::time(nullptr));
	std::unique_ptr<BasePlayer> players[2];
	int simulationsPerMove = 1000;
	Player startingPlayer = Player::RED;
	Window window(800, 800, "Checkers");
	PackedBoard packedBoard;
	MouseHandler mouseHandler(window);
	int timeLimit = std::numeric_limits<int>::max();
	if (argc == 5 || argc == 6)
	{
		Player currentPlayer = Player::RED;
		for (int i = 1; i <= 2; ++i)
		{
			std::string arg(argv[i]);
			if (arg == "gpu")   players[i-1] = std::make_unique<GpuMonteCarloPlayer>(currentPlayer, std::stoi(argv[i + 2]));
			else if (arg == "cpu")   players[i-1] = std::make_unique<CpuMonteCarloPlayer>(currentPlayer, std::stoi(argv[i + 2]));
			else if (arg == "human") players[i-1] = std::make_unique<HumanPlayer>(currentPlayer, mouseHandler);
			else usage(argv[0]);
			currentPlayer = Player::BLACK;
		}
		if (argc == 6) //time limit is specified
		{
			timeLimit = std::stoi(argv[5]);
		}

	}
	else
	{
		usage(argv[0]);
	}




	Game game(packedBoard,
		players[0].get(),
		players[1].get(),
		startingPlayer);
	

	App app(game, window);
	app.SetTimeLimitForGame(timeLimit);
	app.ResetGame(startingPlayer);

	Player winner;
	winner = app.MainLoop();
	std::cerr << winner;
	return winner;
}