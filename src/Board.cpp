#include "Board.h"


Move const Move::InvalidMove = Move(OUT_OF_BOARD, OUT_OF_BOARD);

PackedBoard::PackedBoard()
{
	Reset();
}

PackedBoard::~PackedBoard()
{
}

void PackedBoard::Reset()
{
	std::memset(board, EMPTY, sizeof(board));
	for (int i = 0; i < 12; ++i)
	{
		board[i] = RED_PAWN;
		board[31 - i] = BLACK_PAWN;
	}
}

Piece PackedBoard::GetNeighbourPiece(Index index, Directions direction) const
{
	Index target = GetNeighbourIndex(index, direction);
	if (target == OUT_OF_BOARD) return OUT_OF_BOARD;
	return board[GetNeighbourIndex(index, direction)];
}

Piece PackedBoard::GetPieceAt(Index index) const
{
	return board[index];
}

void PackedBoard::ExecuteMove(const Move & move)
{
	SetPieceAt(move.to, GetPieceAt(move.from));
	SetPieceAt(move.from, EMPTY);
	if (move.capturedField >= 0)
	{
		SetPieceAt(move.capturedField, EMPTY);
	}
	if (move.to > 27 || move.to < 4) UpgradeToKing(move.to);
}

void PackedBoard::SetPieceAt(Index index, Piece newPiece)
{
	board[index] = newPiece;
}

void PackedBoard::UpgradeToKing(Index index)
{
	Piece piece = GetPieceAt(index);
	if (piece == RED_PAWN) SetPieceAt(index, RED_KING);
	if (piece == BLACK_PAWN) SetPieceAt(index, BLACK_KING);
}

PackedBoard * PackedBoard::Clone() const
{
	return new PackedBoard(*this); 
}


Index Board::GetNeighbourIndex(Index index, Directions direction) const
{
	switch (direction)
	{
	case Directions::NE:
		if ((index - 3) % 8 == 0 || index < 4) return OUT_OF_BOARD;
		if (IsEvenRow(index)) return index - 3;
		return index - 4;
	case Directions::NW:
		if ((index-4) % 8 == 0 || index < 4) return OUT_OF_BOARD;
		if (IsEvenRow(index)) return index - 4;
		return index - 5;
	case Directions::SE:
		if ((index - 3) % 8 == 0 || index > 27) return OUT_OF_BOARD;
		if (IsEvenRow(index)) return index + 5;
		return index + 4;
	case Directions::SW:
		if ((index-4) % 8 == 0 || index > 27) return OUT_OF_BOARD;
		if (IsEvenRow(index)) return index + 4;
		return index + 3;
	}
}

bool Move::IsCapturing() const
{
	return capturedField >= 0;
}
