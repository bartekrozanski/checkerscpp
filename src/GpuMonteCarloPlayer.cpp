#include "GpuMonteCarloPlayer.h"



GpuMonteCarloPlayer::GpuMonteCarloPlayer(Player player, int simulationsPerMove) :
	MonteCarloPlayer(
		player,
		simulationsPerMove,
		new RandomPlayer(player),
		new RandomPlayer(BasePlayer::GetOpponentPlayer(player))
	),
	rngState(nullptr)
{
	cudaError_t cudaResult;
	cudaResult = cudaMalloc((void **)&rngState, sizeof(curandState) * NUMBER_OF_BLOCKS * NUM_OF_WARPS);
	if (cudaResult != cudaSuccess)
	{
		throw std::runtime_error("Could not allocate memory on device for RNG state");
	}
	InitializeRNG(std::time(nullptr), rngState);
}


GpuMonteCarloPlayer::~GpuMonteCarloPlayer()
{
	cudaFree(rngState);
	delete myPlayer;
	delete opponentPlayer;
}

double GpuMonteCarloPlayer::Simulate(const Game & game, int iterations)
{
	auto& foo = game.GetBoard();
	int devBoard[MAX_FIELDS];
	for (int i = 0; i < MAX_FIELDS; ++i)
	{
		devBoard[i] = (int)foo.GetPieceAt(i);
	}


	float winRatio = SimulateCuda(devBoard,  rngState, iterations, GetWhichPlayer(),game.WhoseTurn(), game.IsCapturingTurn());
	return winRatio;
}
