#include "RandomPlayer.h"


RandomPlayer::RandomPlayer(Player player):
	BasePlayer(player)
{
}

RandomPlayer::~RandomPlayer()
{
}

Move RandomPlayer::ChooseMove(const Board & board, const Moves & moves)
{
	return moves[std::rand() % moves.size()];
}
