#include "Window.h"


Window::Window(int width, int height, const std::string & title):
	window(sf::VideoMode(width, height), title),
	width(width),
	height(height),
	cellX(width / 8.0f),
	cellY(height / 8.0f)
{
	blackFieldTexture.loadFromFile("./textures/blackField.png");
	whiteFieldTexture.loadFromFile("./textures/whiteField.png");
	possibleMoveTexture.loadFromFile("./textures/possibleMove.png");
	blackPawnTexture.loadFromFile("./textures/blackPawn.png");
	redPawnTexture.loadFromFile("./textures/redPawn.png");
	blackKingTexture.loadFromFile("./textures/blackKing.png");
	redKingTexture.loadFromFile("./textures/redKing.png");

	blackField.setTexture(blackFieldTexture);
	whiteField.setTexture(whiteFieldTexture);
	possibleMove.setTexture(possibleMoveTexture);
	blackPawn.setTexture(blackPawnTexture);
	redPawn.setTexture(redPawnTexture);
	blackKing.setTexture(blackKingTexture);
	redKing.setTexture(redKingTexture);

	float scaleFactorX = (float) width / blackFieldTexture.getSize().x / 8.0f;
	float scaleFactorY = (float) height / blackFieldTexture.getSize().y / 8.0f;
	blackField.setScale(scaleFactorX, scaleFactorY);
	whiteField.setScale(scaleFactorX, scaleFactorY);
	possibleMove.setScale(scaleFactorX, scaleFactorY);
	blackPawn.setScale(scaleFactorX, scaleFactorY);
	redPawn.setScale(scaleFactorX, scaleFactorY);
	blackKing.setScale(scaleFactorX, scaleFactorY);
	redKing.setScale(scaleFactorX, scaleFactorY);



	window.setFramerateLimit(30);
}

void Window::RedrawEverything(const Board & board, const Moves & moves)
{
	Clear();
	DrawBoard(board);
	DrawMoves(moves);
	Display();
}

void Window::DrawBoard(const Board & board)
{
	for (int y = 0; y < 8; ++y)
	{
		for (int x = 0; x < 8; ++x)
		{
			float screenX = x * cellX;
			float screenY = y * cellX;
			sf::Vector2f position(screenX, screenY);
			if ((x + y) % 2)
			{
				blackField.setPosition(position);
				window.draw(blackField);
				Piece piece = board.GetPieceAt(GridCoordsToIndex(x, y));
				switch (piece)
				{
				case BLACK_PAWN:
					blackPawn.setPosition(position);
					window.draw(blackPawn);
					break;
				case RED_PAWN:
					redPawn.setPosition(position);
					window.draw(redPawn);
					break;
				case BLACK_KING:
					blackKing.setPosition(position);
					window.draw(blackKing);
					break;
				case RED_KING:
					redKing.setPosition(position);
					window.draw(redKing);
					break;
				default:
					break;
				}
			}
			else
			{
				whiteField.setPosition(position);
				window.draw(whiteField);
			}
		}
	}
}

void Window::DrawMoves(const Moves & moves)
{

	int x;
	int y;
	sf::Vector2f position;
	for (auto move = moves.cbegin(); move != moves.cend(); ++move)
	{
		//IndexToGridCoords(move->from, x, y);
		//position.x = x * cellX;
		//position.y = y * cellY;
		//possibleMove.setPosition(position);
		//window.draw(possibleMove);

		IndexToGridCoords(move->to, x, y);
		position.x = x * cellX;
		position.y = y * cellY;
		possibleMove.setPosition(position);
		window.draw(possibleMove);
	}
}

void Window::Display()
{
	window.display();
}

void Window::Clear()
{
	window.clear();
}

void Window::HandleInput()
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
			window.close();
	}
}

bool Window::IsOpen()
{
	return window.isOpen();
}

sf::Vector2i Window::GetPosition()
{
	return window.getPosition();
}

Window::~Window()
{
}

Index Window::GridCoordsToIndex(int x, int y)
{
	if (y % 2)
	{
		if (x % 2)return OUT_OF_BOARD;
	}
	else
	{
		if (x % 2 == 0) return OUT_OF_BOARD;
	}
	return (y * 4) + (x/2);
}

void Window::IndexToGridCoords(Index index, int & outX, int & outY)
{

	outY = index / 4;
	outX = (index % 4) * 2;
	if (index % 8 < 4) outX++; //if index points at the even row;
}

void Window::ScreenCoordsToGridCoords(sf::Vector2i screenCoords, int & outX, int & outY)
{
	outX = static_cast<int>(screenCoords.x / cellX);
	outY = static_cast<int>(screenCoords.y / cellY);
}

Index Window::ScreenCoordsToIndex(sf::Vector2i screenCoords)
{
	int x, y;
	ScreenCoordsToGridCoords(screenCoords, x, y);
	return GridCoordsToIndex(x, y);
}

