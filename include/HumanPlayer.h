#pragma once
#include "BasePlayer.h"
#include "Window.h"
#include "MouseHandler.h"
class HumanPlayer :
	public BasePlayer
{
public:
	HumanPlayer(Player player, MouseHandler& mouseHandler);
	virtual ~HumanPlayer() override;

	virtual Move ChooseMove(const Board& board, const Moves & moves) override;

private:
	MouseHandler& mouseHandler;
	Index from;
	Index to;
};

