#pragma once
#include "BasePlayer.h"
#include <cstdlib>
#include <ctime>
class RandomPlayer :
	public BasePlayer
{
public:
	RandomPlayer(Player player);
	virtual ~RandomPlayer() override;
	virtual Move ChooseMove(const Board & board, const Moves & moves) override;
};

