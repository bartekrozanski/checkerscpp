#include "MouseHandler.h"



MouseHandler::MouseHandler(Window& window):
	window(window),
	from(OUT_OF_BOARD),
	wasReleased(true)
{
}


MouseHandler::~MouseHandler()
{
}

Move MouseHandler::HandleInput()
{
	std::cout << "Selected field: " << (int)from << "\n";
	if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Left))
	{
		if (wasReleased)
		{
			wasReleased = false;
			sf::Vector2i position = sf::Mouse::getPosition() - window.GetPosition();
			Index index = window.ScreenCoordsToIndex(position);
			if (index != OUT_OF_BOARD)
			{
				if (from != OUT_OF_BOARD)
				{
					Move move(from, index);
					from = OUT_OF_BOARD;
					return move;
				}
				from = index;
			}
		}
	}
	else
	{
		wasReleased = true;
	}
	return Move::InvalidMove;
}
