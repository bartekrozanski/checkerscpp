#include "App.h"



App::App( Game& game, Window & window):
	game(game),
	window(window)
{
}

App::~App()
{
}


Player App::MainLoop()
{
	clock.restart();
	window.RedrawEverything(game.GetBoard(), game.GetMoves());
	while (window.IsOpen() && clock.getElapsedTime().asSeconds() < timeLimit)
	{
		window.RedrawEverything(game.GetBoard(), game.GetMoves());
		HandleInput();
		if (game.MakePly() == true)
		{
			DisplayWhoWon();
			break;
		}
	}
	return game.WhoWon();
}

void App::DisplayWhoWon()
{
	if (game.WhoWon() == Player::BLACK)
	{
		std::cout << "Player Black won!\n";
	}
	else
	{
		std::cout << "Player Red won!\n";
	}
}

void App::SetTimeLimitForGame(int seconds)
{
	timeLimit = seconds;
}

void App::ResetGame(Player startingPlayer)
{
	game.Reset(startingPlayer);
}

void App::HandleInput()
{
	window.HandleInput();
}
