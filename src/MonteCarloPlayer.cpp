#include "MonteCarloPlayer.h"




MonteCarloPlayer::MonteCarloPlayer(Player player,
	int simulationsPerMove,
	BasePlayer* myPlayerForSimulation,
	BasePlayer* opponentPlayerForSimulation) :
	BasePlayer(player),
	simulationsPerMove(simulationsPerMove),
	myPlayer(myPlayerForSimulation),
	opponentPlayer(opponentPlayerForSimulation)
{
}

MonteCarloPlayer::~MonteCarloPlayer()
{
}


Move MonteCarloPlayer::ChooseMove(const Board & board, const Moves & moves)
{
	MoveGenerator moveGenerator;
	
	std::cout << "\n" << (GetWhichPlayer() == Player::RED ? "Red player " : "Black player ") << "simulations outcome:\n";
	double bestWinRatio = -1;
	Move bestMove = Move::InvalidMove;

	std::chrono::microseconds totalTime(0);

	for (auto it = moves.begin(); it != moves.cend(); ++it)
	{
		Game game(board, myPlayer, opponentPlayer, GetWhichPlayer());
		game.ExecuteMove(*it);


		auto start = std::chrono::high_resolution_clock::now();
		double winRatio = Simulate(game, simulationsPerMove);
		auto stop = std::chrono::high_resolution_clock::now();
		auto simulationTime = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
		totalTime += simulationTime;

		std::cout << winRatio << " win probability with move from " << (int)it->from << " to " << (int)it->to << "\n";
		if (winRatio > bestWinRatio)
		{
			bestMove = *it;
			bestWinRatio = winRatio;
		}
	}

	auto totalSimulations = simulationsPerMove * moves.size();
	std::cout << totalSimulations << " simulations ran with speed of " << totalSimulations * 1e6 / (double)totalTime.count() << " games per second\n";
	return bestMove;
}