#include "Board.h"

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <device_functions.h>
#include <cstdlib>
#include <stdio.h>
#include <curand.h>
#include <curand_kernel.h>


#define MAX_FIELDS 32
#define MAX_MOVES 4
#define THREADBLOCK_SIZE 256
#define THREADS_PER_GAME 32
#define NUMBER_OF_BLOCKS 32
#define NUM_OF_WARPS (THREADBLOCK_SIZE/THREADS_PER_GAME)

#define BLACK_PLAYER 1
#define RED_PLAYER 0
#define INVALID_INDEX -1
#define MAIN_THREAD 0

__global__ void simulateKernel(int board[MAX_FIELDS],
	curandState* rngState,
	int numOfSimulations,
	float* dev_winRatios,
	int myPlayer,
	int whoseTurn,
	int isCapturingTurn);

__global__ void initializeRng(int seed, curandState *rngState);



__device__ __forceinline__ int getIndex(int thread_id,
	int logical_index);
__device__ __forceinline__ int getIndex(int thread_it);
__device__ int getNorthWestIndex(int index);
__device__ int getNorthEastIndex(int index);
__device__ int getSouthWestIndex(int index);
__device__ int getSouthEastIndex(int index);
__device__ int isEvenRow(int index);
__device__ int isMainThread(int tid);
__device__ int getOffset(int tid);

float SimulateCuda(int board[],
	curandState *rngState,
	int numOfSimulations,
	int myPlayer,
	int whoseTurn,
	int isCapturingTurn);
void InitializeRNG(int seed, curandState *rngState);


