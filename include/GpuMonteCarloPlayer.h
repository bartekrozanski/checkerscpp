#pragma once
#include "MonteCarloPlayer.h"
#include <iostream>
#include "Game.h"
#include "Simulation.cuh"
#include <curand_kernel.h>

#include "RandomPlayer.h"


class GpuMonteCarloPlayer :
	public MonteCarloPlayer
{
public:
	GpuMonteCarloPlayer(Player player, int simulationsPerMove);
	virtual ~GpuMonteCarloPlayer() override;

	virtual double Simulate(const Game& game, int iterations) override;

protected:
	curandState *rngState;
};

