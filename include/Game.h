#pragma once
#include "BasePlayer.h"
#include "Board.h"
class Game
{
public:
	Game(const Board &board,
		BasePlayer* playerRed,
		BasePlayer* playerBlack,
		Player startingPlayer);

	Game(const Game& other);

	~Game();

	void Reset(Player startingPlayer);
	void ExecuteMove(const Move& move);
	bool MakePly();
	Player WhoWon();
	const Board& GetBoard() const;
	const Moves& GetMoves();
	bool IsCapturingTurn() const;
	Player WhoseTurn() const;

protected:
	void NextTurn();
	Player WaitingPlayer();
	bool CheckIfPlayerLost(Player player);

	bool capturingTurn;
	Index indexOfCapturingPiece;
	Player currentPlayer;
	Board *board;
	BasePlayer *players[2];
	MoveGenerator moveGenerator;
};

