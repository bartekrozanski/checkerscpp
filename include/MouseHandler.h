#pragma once
#include "Window.h"
#include <iostream>
class MouseHandler
{
public:
	MouseHandler(Window& window);
	~MouseHandler();
	Move HandleInput();
private:
	Index from;
	bool wasReleased;
	Window& window;
	
};

