#pragma once
#include <vector>
#include "Board.h"

using Moves = std::vector<Move>;

enum Player {
	RED,
	BLACK,
	NONE
};



class MoveGenerator
{
public:
	void UpdateAllPossibleMoves(const Board& board, Player player);
	void UpdateOnlyCapturingMovesForGivenIndex(const Board& board, Index target, Player player);
	bool PossibleMoves();
	const Moves& GetMoves();
protected:
	void AddCapturingMoveInDirection(Index from, Player player, const Board& board, Directions direction);
	void AddMoveInDirection(Index from, const Board& board, Directions direction);

	void AddCapturingMovesForPiece(Index from, Player player, const Board& board);
	void AddMovesForPiece(Index from, const Board& board);
	Moves moves;
};

