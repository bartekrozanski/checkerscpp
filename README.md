# What is it?
Checkers game with AI using pure monte carlo method.

AI can use CPU or GPU to perfmorm simulations.
GPU computations are powered by nVidia CUDA.

For example, you can run this app with both players specified as AI players to observe a game between them - one using CPU and the other one GPU to decide upon best strategy.
You can just try yourself in a game with pure computing power of GPU :)
It depends on hardware, but even on middle class notebook with nVidia mx250, gpu is about 20 times faster than CPU in simulating checkers games.

# What does it look like?
<img src="screenshots/screenshot1.PNG" width="600" height="300" />

Playable fields (black ones) are indexed from left to right, from top to bottom starting with numbers \{0,1,2,...,31\}.

# Usage
Run this app without args to see usage instructions.

# Building this project
Firstly - this project was tested on Windows as it uses SFML build for this platform.
1. Clone this repo
2. Download [SFML](https://www.sfml-dev.org/download/sfml/2.5.1/) (Use 64-bit version)
3. Place `SFML-2.5.1` folder in the root directory of cloned repo
4. You are ready to go! Open root directory with Visual Studio, wait for CMake to do its job and build the project
5. (Optional) From default, project will build in debug mode. Switch to release mode in Visual Studio to greatly increase performance.
