#pragma once
#include "MoveGenerator.h"



class BasePlayer
{
public:
	BasePlayer(Player player): player(player) {};
	virtual ~BasePlayer() {};
	virtual Move ChooseMove(const Board& board, const Moves& moves) = 0;
	Player GetWhichPlayer();

	static Player GetOpponentPlayer(Player myPlayer);


private:
	Player player;
};

