#pragma once
#include "BasePlayer.h"
#include <chrono>
#include <iostream>
#include <ctime>
#include "Game.h"

class MonteCarloPlayer :
	public BasePlayer
{
public:
	MonteCarloPlayer(Player player,
		int simulationsPerMove,
		BasePlayer* myPlayerForSimulation,
		BasePlayer* opponentPlayerForSimulation);

	virtual Move MonteCarloPlayer::ChooseMove(const Board & board, const Moves & moves) override;

	virtual ~MonteCarloPlayer() = 0;

protected:
	virtual double Simulate(const Game& game, int iterations) = 0;
	int simulationsPerMove;
	BasePlayer *myPlayer;
	BasePlayer *opponentPlayer;
};

