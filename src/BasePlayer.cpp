#include "BasePlayer.h"

Player BasePlayer::GetWhichPlayer()
{
	return player;
}

Player BasePlayer::GetOpponentPlayer(Player myPlayer)
{
	switch (myPlayer)
	{
	case(Player::RED):
		return Player::BLACK;
	case(Player::BLACK):
		return Player::RED;
	default:
		return Player::NONE;
	}
}
