#include "MoveGenerator.h"

void MoveGenerator::UpdateAllPossibleMoves(const Board & board, Player player)
{
	moves.clear();
	Piece piece;
	switch (player)
	{
	case RED:
		for (Index i = 0; i < FIELDS; ++i)
		{
			piece = board.GetPieceAt(i);
			if (piece == RED_PAWN || piece == RED_KING)
			{
				AddCapturingMovesForPiece(i, player, board);
			}
		}
		break;
	case BLACK:
		for (Index i = 0; i < FIELDS; ++i)
		{
			piece = board.GetPieceAt(i);
			if (piece == BLACK_PAWN || piece == BLACK_KING)
			{
				AddCapturingMovesForPiece(i, player, board);
			}
		}
		break;
	}
	if (!moves.empty()) return;
	switch (player)
	{
	case RED:
		for (Index i = 0; i < FIELDS; ++i)
		{
			piece = board.GetPieceAt(i);
			if (piece == RED_PAWN || piece == RED_KING)
			{
				AddMovesForPiece(i, board);
			}
		}
		break;
	case BLACK:
		for (Index i = 0; i < FIELDS; ++i)
		{
			piece = board.GetPieceAt(i);
			if (piece == BLACK_PAWN || piece == BLACK_KING)
			{
				AddMovesForPiece(i, board);
			}
		}
		break;
	}
}

void MoveGenerator::UpdateOnlyCapturingMovesForGivenIndex(const Board & board, Index target, Player player)
{
	moves.clear();
	AddCapturingMovesForPiece(target, player, board);
}

bool MoveGenerator::PossibleMoves()
{
	return !moves.empty();
}

const Moves & MoveGenerator::GetMoves()
{
	return moves;
}

void MoveGenerator::AddCapturingMoveInDirection(Index from, Player player, const Board & board, Directions direction)
{
	Index otherIndex;
	Piece otherPiece = board.GetNeighbourPiece(from, direction);
	switch (player)
	{
	case RED:
		if (otherPiece == BLACK_PAWN || otherPiece == BLACK_KING)
		{
			otherIndex = board.GetNeighbourIndex(from, direction);
			otherPiece = board.GetNeighbourPiece(otherIndex, direction);
			if (otherPiece == EMPTY)
			{
				moves.push_back(Move(from, board.GetNeighbourIndex(otherIndex, direction), otherIndex));
			}
		}
		break;
	case BLACK:
		if (otherPiece == RED_PAWN || otherPiece == RED_KING)
		{
			otherIndex = board.GetNeighbourIndex(from, direction);
			otherPiece = board.GetNeighbourPiece(otherIndex, direction);
			if (otherPiece == EMPTY)
			{
				moves.push_back(Move(from, board.GetNeighbourIndex(otherIndex, direction), otherIndex));
			}
		}
		break;
	}
}

void MoveGenerator::AddMoveInDirection(Index from, const Board & board, Directions direction)
{
	if (board.GetNeighbourPiece(from, direction) == EMPTY)
	{
		moves.push_back(Move(from, board.GetNeighbourIndex(from, direction)));
	}
}

void MoveGenerator::AddCapturingMovesForPiece(Index from, Player player, const Board & board)
{
	Piece piece = board.GetPieceAt(from);
	switch (piece)
	{
	case BLACK_PAWN:
		AddCapturingMoveInDirection(from, player, board, Directions::NE);
		AddCapturingMoveInDirection(from, player, board, Directions::NW);
		break;
	case RED_PAWN:
		AddCapturingMoveInDirection(from, player, board, Directions::SE);
		AddCapturingMoveInDirection(from, player, board, Directions::SW);
		break;
	case BLACK_KING:
	case RED_KING:
		AddCapturingMoveInDirection(from, player, board, Directions::NE);
		AddCapturingMoveInDirection(from, player, board, Directions::NW);
		AddCapturingMoveInDirection(from, player, board, Directions::SE);
		AddCapturingMoveInDirection(from, player, board, Directions::SW);
		break;
	}
}

void MoveGenerator::AddMovesForPiece(Index from, const Board & board)
{
	Piece piece = board.GetPieceAt(from);
	switch (piece)
	{
	case BLACK_PAWN:
		AddMoveInDirection(from, board, Directions::NW);
		AddMoveInDirection(from, board, Directions::NE);
		break;
	case RED_PAWN:
		AddMoveInDirection(from, board, Directions::SW);
		AddMoveInDirection(from, board, Directions::SE);
		break;
	case BLACK_KING:
	case RED_KING:
		AddMoveInDirection(from, board, Directions::NW);
		AddMoveInDirection(from, board, Directions::NE);
		AddMoveInDirection(from, board, Directions::SW);
		AddMoveInDirection(from, board, Directions::SE);
		break;
	}
}
