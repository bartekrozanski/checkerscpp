#include "Game.h"





Game::Game(const Board & board,
	BasePlayer *player1,
	BasePlayer *player2,
	Player startingPlayer):
	moveGenerator(moveGenerator),
	currentPlayer(startingPlayer)
{
	players[player1->GetWhichPlayer()] = player1;
	players[player2->GetWhichPlayer()] = player2;
	this->board = board.Clone();
}

Game::Game(const Game & other) :
	capturingTurn(other.capturingTurn),
	indexOfCapturingPiece(other.indexOfCapturingPiece),
	currentPlayer(other.currentPlayer),
	players{other.players[0], other.players[1]},
	moveGenerator(other.moveGenerator)
{
	board = other.board->Clone();
}

Game::~Game()
{
	delete board;
}

void Game::Reset(Player startingPlayer)
{
	board->Reset();
	capturingTurn = false;
	currentPlayer = startingPlayer;
}

void Game::ExecuteMove(const Move & move)
{
	board->ExecuteMove(move);
	if (move.IsCapturing())
	{
		capturingTurn = true;
		indexOfCapturingPiece = move.to;
	}
	else
	{
		capturingTurn = false;
		NextTurn();
	}
}

bool Game::MakePly()
{
	BasePlayer *player = players[WhoseTurn()];
	if (capturingTurn)
	{
		moveGenerator.UpdateOnlyCapturingMovesForGivenIndex(*board, indexOfCapturingPiece, WhoseTurn());
		if (!moveGenerator.PossibleMoves())
		{
			NextTurn();
			capturingTurn = false;
			return false;
		}
	}
	else
	{
		moveGenerator.UpdateAllPossibleMoves(*board, WhoseTurn());
		if (!moveGenerator.PossibleMoves())
		{
			return true;
		}
	}

	Move move = player->ChooseMove(*board, moveGenerator.GetMoves());
	if (move.from == OUT_OF_BOARD) return false;
	ExecuteMove(move);
	return false;
}



Player Game::WhoWon()
{
	if (CheckIfPlayerLost(Player::RED)) return Player::BLACK;
	if (CheckIfPlayerLost(Player::BLACK)) return Player::RED;
	return Player::NONE;

}

const Board & Game::GetBoard() const
{
	return *board;
}

const Moves & Game::GetMoves()
{
	return moveGenerator.GetMoves();
}

bool Game::IsCapturingTurn() const
{
	return capturingTurn;
}

Player Game::WhoseTurn() const
{
	return currentPlayer;
}

void Game::NextTurn()
{
	currentPlayer = currentPlayer == Player::RED ? Player::BLACK : Player::RED;
}

Player Game::WaitingPlayer()
{
	return WhoseTurn() == Player::RED ? Player::BLACK : Player::RED;
}

bool Game::CheckIfPlayerLost(Player player)
{
	moveGenerator.UpdateAllPossibleMoves(*board, player);
	if (!moveGenerator.PossibleMoves()) return true;
	return false;

}
