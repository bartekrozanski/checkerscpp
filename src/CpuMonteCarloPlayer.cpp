#include "CpuMonteCarloPlayer.h"


CpuMonteCarloPlayer::CpuMonteCarloPlayer(Player player, int simulationsPerMove):
	MonteCarloPlayer(player,
		simulationsPerMove,
		new RandomPlayer(player),
		new RandomPlayer(BasePlayer::GetOpponentPlayer(player)))
{
}

CpuMonteCarloPlayer::~CpuMonteCarloPlayer()
{
	delete myPlayer;
	delete opponentPlayer;
}

double CpuMonteCarloPlayer::Simulate(const Game& game, int iterations)
{
	int wins = 0;
	for (int i = 0; i < iterations; ++i)
	{
		Game currentGame(game);
		while (currentGame.MakePly() == false);
		if (currentGame.WhoWon() == GetWhichPlayer())
		{
			wins++;
		}

	}
	return (double)wins / (double)iterations;
}
